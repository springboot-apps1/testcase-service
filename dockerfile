FROM openjdk:11
EXPOSE 9091
ADD target/testcase-service-0.0.1-SNAPSHOT.jar testcase-service-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/testcase-service-0.0.1-SNAPSHOT.jar"]